FROM node:14
WORKDIR /var/app
copy . /var/app
RUN npm install
CMD ["node", "app.js"]

